module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/essential"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly",
        "require": true
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module",
        "extends": "standard",
	    "parser": "babel-eslint"
    },
    "plugins": [
        "vue"
    ],
    "rules": {
    }
};