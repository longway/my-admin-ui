var axios = require("axios");
var MockAdapter = require("axios-mock-adapter");

var mock = new MockAdapter(axios);

let users = [
    { id: 1, name: "张三", email: "zhangsan@dd.com", created_at: '2020-1-1 12:00:00'}
]

mock.onPost('/login').reply(200, {
    token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ'
})

mock.onGet('/me').reply(200, {
    data: {
        name: '林康贵',
        email: 'linkanggui1@gmail.com'
    }
})

mock.onGet("/staffs").reply(200, {
    data: users,
    meta: {
        pagination: {
            total: 5,
            count: 5,
            per_page: 10,
            current_page: 1,
            total_pages: 1,
            links: []
        }
    }
})

mock.onPost("/staffs").reply(422, {
    status_code: 422,
    message: "422 Unprocessable Entity",
    errors: {
        email: [
            '该邮箱已被注册'
        ]
    }
})

mock.onGet('/roles').reply(200, {
    data: [
        {id: 1, role: '管理员', created_at: '2020-1-1 12:00:00'}
    ],
    meta: {
        pagination: {
            total: 5,
            count: 5,
            per_page: 10,
            current_page: 1,
            total_pages: 1,
            links: []
        }
    }
})

mock.onGet('/permissions').reply(200, {
    data: [
        {id: 1, permission: '获取人员列表', key: 'staff.query', api_url: '/staffs', api_method: 'GET', created_at: '2020-1-1 12:00:00'},
        {id: 1, permission: '添加人员', key: 'staff.create', api_url: '/staffs', api_method: 'POST', created_at: '2020-1-1 12:00:00'},
        {id: 1, permission: '删除人员', key: 'staff.delete', api_url: '/staffs/{id}', api_method: 'DELETE', created_at: '2020-1-1 12:00:00'}
    ],
    meta: {
        pagination: {
            total: 5,
            count: 5,
            per_page: 10,
            current_page: 1,
            total_pages: 1,
            links: []
        }
    }
})

mock.onGet('/menus').reply(200, {
    data: [
        {
            id: 1,
            title: '基础管理',
            type: 'group',
            children: [
                {
                    id: 2,
                    title: '人员管理',
                    type: 'route',
                    url: '/staff',
                    children: [
                        {
                            id: 7,
                            title: '添加人员',
                            type: 'hidden',
                            url: '/staff/create' 
                        }
                    ]
                },
                {
                    id: 3,
                    title: '角色管理',
                    type: 'route',
                    url: '/roles',
                    children: [
                        {
                            id: 8,
                            title: '添加角色',
                            type: 'hidden',
                            url: '/roles/create' 
                        }
                    ]
                },
                {
                    id: 4,
                    title: '菜单管理',
                    type: 'route',
                    url: '/menus'
                },
                {
                    id: 5,
                    title: '权限管理',
                    type: 'route',
                    url: '/permissions'
                }
            ]
        }
    ]
})