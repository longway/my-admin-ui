export default {
    data () {
        return {
            pagination: {
                currentPage: 0,
                total: 0,
                pageSizes: [10, 20, 50],
                pageSize: 10
            }
        }
    },
    methods: {
        setPagination(data) {
            this.pagination.total = data.total
            this.pagination.currentPage = data.current_page
        }
    }
}