import { mapState } from 'vuex'

const findMenu = (menus, path, parent = null) => {
    for (let item of menus) {
        if (item.type == 'group') {
            const result = findMenu(item.children, path, item)
            if (result) {
                return result
            }
        } else if (item.type == 'route') {
            if (item.url == path) {
                item.parent = parent
                return item
            }
        }
    }
    return null
}

const whileMenu = (menus, handler) => {
    menus.forEach(item => {
        if (item instanceof Object) {
            if (item.url) {
                handler(item)
            }
            if (item.children) {
                whileMenu(item.children, handler)
            }
        }
    })
}

export default {
    computed: {
        ...mapState({
            menus: state => state.basics.menus
        }),
        allRoutes() {
            let routes = []
            whileMenu(this.menus, item => {
                routes.push(item)
            })
            return routes
        }
    },
    methods: {
        getBreadcrumbs (path) {
            let menu = findMenu(JSON.parse(JSON.stringify(this.menus)), path)
            if (menu) {
                let breadcrumbs = []
                breadcrumbs.unshift({title: menu.name})
                while (menu.parent) {
                    menu = menu.parent
                    breadcrumbs.unshift({title: menu.name})
                }
                return breadcrumbs
            }
            return null
        }
    }
}