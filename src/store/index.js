const state = {
    menus: [],
    me: {
        name: '',
        email: ''
    },
    loggedIn: false,
    token: ''
}

const mutations = {
    setMenus (state, menus) {
        state.menus = menus
        state.menus.push({
            name: '登录',
            url: '/login',
            type: 'hidden'
        })
        state.menus.push({
            name: '403',
            url: '/403',
            type: 'hidden'
        })
    },
    setMe (state, { name, email }) {
        state.me.name = name
        state.me.email = email
        state.loggedIn = true
    },
    setToken (state, token) {
        state.token = token
        localStorage.setItem('token', token)
    },
    setLoggedIn (state) {
        state.loggedIn = false
    }
}

export default {
    namespaced: true,
    state,
    mutations
}