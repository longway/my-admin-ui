const router = {
    routes: [
        {
            path: '/login',
            name: 'login',
            component: () => import('./views/Login.vue'),
            meta: { requiresAuth: false }
        },
        {
            path: '/',
            component: () => import('./components/AdminLayout.vue'),
            children: [
                {
                    path: '/staff',
                    name: 'staff',
                    component: () => import('./views/Staff.vue')
                },
                {
                    path: '/staff/create',
                    name: 'staff.create',
                    component: () => import('./views/CreateStaff.vue')
                },
                {
                    path: '/staff/update',
                    name: 'staff.update',
                    component: () => import('./views/UpdateStaff.vue')
                },
                {
                    path: '/roles',
                    name: 'role',
                    component: () => import('./views/Role.vue')
                },
                {
                    path: '/roles/create',
                    name: 'role.create',
                    component: () => import('./views/CreateRole.vue')
                },
                {
                    path: '/roles/update',
                    name: 'role.update',
                    component: () => import('./views/UpdateRole.vue')
                },
                {
                    path: '/permissions',
                    name: 'permission',
                    component: () => import('./views/Permission.vue')
                },
                {
                    path: '/menus',
                    name: 'menu',
                    component: () => import('./views/Menu.vue')
                },
                {
                    path: '/403',
                    name: '403',
                    component: () => import('./views/403.vue')
                }
            ]
        },
    ],
    add: (item) => {
        router.routes[1].children.push(item)
    }
}
export default router