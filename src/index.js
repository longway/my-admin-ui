import MyLayout from './components/MyLayout'
import MyPage from './components/MyPage'
import MyFormPage from './components/MyFormPage'
import MyBreadcrumb from './components/MyBreadcrumb'
import router from './route.js'
import store from './store/index.js'
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'


Vue.use(ElementUI)

const enableMock = () => {
    require('./mock/mock.js')
}

const initAxios = (baseURL) => {
    axios.defaults.baseURL = baseURL
}

export {
    MyLayout,
    MyPage,
    MyFormPage,
    MyBreadcrumb,
    router,
    store,
    axios,
    initAxios,
    enableMock
}