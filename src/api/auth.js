import axios from 'axios'

export default {
    login (data, cb, errorCb) {
        axios.post('/login', data).then(response => {
            const data = response.data
            cb(data)
        }).catch(error => {
            if (errorCb) {
                errorCb(error.response.data)
            }
        })
    },
    logout (token, cb) {
        axios.get('/logout', {
            params: {
                token
            }
        }).then(() => {
            cb()
        })
    }
}