import axios from 'axios'

export default {
    query (cb, errorCb) {
        axios.get('/menus').then(response => {
            cb(response.data)
        }).catch(error => {
            if (errorCb) {
                errorCb(error.response.data)
            }
        })
    },
    store (data, cb, errorCb) {
        axios.post('/menus', data).then(() => {
            cb()
        }).catch(error => {
            if (errorCb) {
                errorCb(error.response.data)
            }
        })
    },
    destroy (id, cb, errorCb) {
        axios.delete(`/menus/${id}`).then(() => {
            cb()
        }).catch(error => {
            if (errorCb) {
                errorCb(error.response.data)
            }
        })
    }
}