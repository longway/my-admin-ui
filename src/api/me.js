import axios from 'axios'

export default {
    query (cb, errorCb) {
        axios.get('/me').then(response => {
            const data = response.data
            const { name, email } = data.data
            cb({
                name,
                email
            })
        }).catch(error => {
            if (errorCb) {
                errorCb(error.response.data)
            }
        })
    }
}